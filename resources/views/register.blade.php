<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="nama1"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="nama2"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesian</option>
            <option value="singapura">Singapore</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select><br><br>
        <label>Languange Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit">
    </form>
    
</body>
</html>